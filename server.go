package main

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func main() {
	e := echo.New()
	e.GET("/test", func(c echo.Context) error {
		return c.String(http.StatusOK, "ok")
	})
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, KKKKKKK!")
	})
	e.Logger.Fatal(e.Start(":1323"))
}