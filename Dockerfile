# the different stages of this Dockerfile are meant to be built into separate images
# https://docs.docker.com/develop/develop-images/multistage-build/#stop-at-a-specific-build-stage
# https://docs.docker.com/compose/compose-file/#target


# https://docs.docker.com/engine/reference/builder/#understand-how-arg-and-from-interact
ARG ALPINE_VERSION=3.12.0

FROM alpine:${ALPINE_VERSION} AS backend

RUN apk add --no-cache go
# Configure Go
ENV GO111MODULE on

WORKDIR /srv/api

RUN mkdir -p bin

# copy src
COPY . /srv/api

# Install
RUN go get -u

RUN go build -o bin/democicd server.go

RUN chmod +x bin/democicd

COPY entrypoint.sh /usr/local/bin/docker-entrypoint

RUN chmod +x /usr/local/bin/docker-entrypoint

ENTRYPOINT ["docker-entrypoint"]