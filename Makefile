build:
	go build -o bin/democicd server.go
run:
	go run main.go
compile:
	echo "Compiling for every OS and Platform"
	GOOS=linux GOARCH=amd64 go build -o bin/democicd-linux-amd64 server.go
all: build compile