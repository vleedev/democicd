#!/bin/sh
workDir=$(pwd)
gitlabRegistryControllerFile="${workDir}/gitlabRegistryController"
if [ ! -s "${gitlabRegistryControllerFile}" ]; then
    curl --request GET -sL \
          --url 'https://github.com/vleedev/gitlabRegistryController/releases/download/0.4.9/gitlabRegistryController-linux-amd64'\
          --output "${gitlabRegistryControllerFile}"
fi
chmod 755 "${gitlabRegistryControllerFile}"